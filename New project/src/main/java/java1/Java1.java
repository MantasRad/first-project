package java1;

public class Java1 {
    public static void main(String[] args) {

        /*
        Sukurti metoda, kuris priima sveika skaiciu ir ji grazina
         */
        Java1 uzd = new Java1();
        uzd.takeAndReturnNumber(10);

        /*
        Sukurti metoda, kuris priima sveika skaiciu, atspausdina jo reiksme ir ji grazina.
         */
        uzd.takePrintAndReturn(20);

        /*
        Sukurti metoda, kuris priima du sveikus skaicius ir grazina ju suma.
         */
        uzd.takeTwoNumbersAndReturnSum(25, 35);

        /*
        Sukurti metoda, kuris priima du simbolius (char) ir kaip String grazina sujungtus simbolius. Rezultata
        atspausdinti.
         */
        uzd.takeSixSimbolsAndReturnAsString('M', 'a', 'n', 't', 'a', 's');

        /*
        Sukurti metoda, kuris sudeda sveika skaiciu su perduodamu String kuris negrazina jokios reiksmes.
         */
        //metod = no value;
        uzd.addIntAndStringWithoutValue(15, "Mantas");

        /*
        Sukurti metoda, kuris nepriima nei vieno parametro ir grazina boolean true reiksme.
         */
        uzd.noParameter();
        System.out.println(uzd.noParameter());

    }
    public int takeAndReturnNumber(int number1){
        return number1;
    }
    public int takePrintAndReturn (int number2){
        System.out.println("Priimtas sveikas skaicius yra: " + number2);
        System.out.println("=========================================");
        return number2;
    }
    public int takeTwoNumbersAndReturnSum (int number3, int number4){
        int number5 = number3 + number4;
        System.out.println("Dvieju skaiciu suma yra: " + number5);
        System.out.println("=========================================");
        return number5;
    }
    public String takeSixSimbolsAndReturnAsString(char ch1, char ch2, char ch3, char ch4, char ch5, char ch6){
        String ch7 = String.valueOf(ch1) + String.valueOf(ch2) + String.valueOf(ch3) + String.valueOf(ch4) + String.valueOf(ch5) + String.valueOf(ch6);
        System.out.println("Grazinamas Stringas yra: " + ch7);
        System.out.println("=========================================");
        return ch7;
    }
    public static String addIntAndStringWithoutValue(int anyNumber, String anyWord){
        String anyResult = String.valueOf(anyNumber) + anyWord;
        System.out.println(anyResult);
        System.out.println("=========================================");
        return anyResult;
    }
    public boolean noParameter (){
        boolean anything = true;
        return anything;
    }


}
