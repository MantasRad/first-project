package PirmaPaskaita;

public class UzduotysPirmadienis {

    public static void main(String args[]){
//        1. Sukurti int kintamaji, priskirti jam pradine reiksme. Atspausdinti kintamojo ir kintamojo +1 reiksmes.
        int skaicius = 6;
        int skaicius2 = skaicius + 1;
        int skaicius3 = 3;

        System.out.println(skaicius);
        System.out.println(skaicius2);

//        2. Atlikti sveiku skaiciu dalyba.
        int skaicius4 = skaicius / skaicius3;
        System.out.println(skaicius4);

//        3. Sudauginti du sveikus skaicius, prasmingai atspausdinti rezultata.
        int skaicius5 = skaicius * skaicius3;
        System.out.println(skaicius5);

//        4. Sukurti String kintamaji su pradine reiksme, atspausdinti jo reiksme.
        String zodis = "labas";
        System.out.println(zodis);

//        5. Prie String prideti kita String kintamaji, atspausdinti sudeties rezultata.
        String zodis2 = "vakaras";
        System.out.println(zodis + " " + zodis2);

//        6. Sukurti realaus skaiciaus tipo kintamaji. Atspausdinti. Prideti kita realu skaiciu ir ji atspausdinti.
        double pirmas = 1.5;
        System.out.println(pirmas);
        double antras = 3.7;
        System.out.println(pirmas + antras);

//        metod1
        UzduotysPirmadienis uzd = new UzduotysPirmadienis();
        int value = sveikasSkaicius(5);
//        metod2
        uzd.sveikasSkaicius2(6);
//        metod3
//        uzd.sveikasSkaicius3(tab3: 11, tab4: 25);
////        metod4
//        uzd.duSimbol(ch1: 'a', ch2: 'b');
////        metod5
////        void
////        metod6
//        System.out.println(uzd.beparametris());


    }

    public static int sveikasSkaicius(int skaicius){
        return skaicius;
    }

    public static int sveikasSkaicius2(int skaicius2){
        return skaicius2;
    }

}
