package java3;

public class UzduotysSalygosSakiniai {

    public static void main(String[] args) {

        /*
        Sukurti boolean kintamaji, priskirti jam pradine reiksme. Sukurti sakini, kuris jeigu kintamojo reiksme true -
        atspausdintu "Tiesa", jeigu false - atspausdintu "Netiesa"
         */
        boolean tiesa = true;

        if(tiesa == true){
            System.out.println("Tiesa");
            System.out.println("===================================================================");
        }else {
            System.out.println("Netiesa");
            System.out.println("===================================================================");
        }

        /*
        Parasyti salygos sakini, kuris atspausdintu pranesima jeigu kintamojo reiksme didesne uz 100.
         */
        int number = 199;

        if(number > 100){
            System.out.println("Skaicius yra didesnis uz 100");
            System.out.println("===================================================================");
        }

        /*
        Parasyti salygos sakini, kuris atspausdintu pranesima jeigu kintamojo reiksme didesne uz 100,
        jeigu ne - tada atspausdintu kita pranesima.
         */
        if(number > 100){
            System.out.println("Kintamojo reiksme yra didesne uz 100");
            System.out.println("===================================================================");
        }else {
            System.out.println("Kintamojo reiksme yra mazesne uz 100");
            System.out.println("===================================================================");
        }

        /*
        Parasyti salygos sakini, kuris patikrintu ir atspausdintu pranesima "Skaicius yra 0...100 tarpe" jeigu
        kintamojo reiksme yra atitinkame rezyje. Kitu atveju atspausdintu pranesima "Skaiciaus apibreztame rezyje nera"
         */
        if(number >= 0 && number <= 100){
            System.out.println("Skaicius yra 0...100 tarpe");
            System.out.println("===================================================================");
        }else {
            System.out.println("Skaiciaus apibreztame rezyje nera");
            System.out.println("===================================================================");
        }


    }
}
