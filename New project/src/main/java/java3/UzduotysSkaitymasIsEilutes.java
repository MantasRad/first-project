package java3;

import java.util.Scanner;

public class UzduotysSkaitymasIsEilutes {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        UzduotysSkaitymasIsEilutes sk = new UzduotysSkaitymasIsEilutes();
        sk.readPrintSumAndReturn();



     /*
     Nuskaityti skaiciu ir ji atspausdinti.
      */
//        System.out.println("Iveskite skaiciu, kuri norite atspausdinti!");
//        int number1 = scanner.nextInt();
//        System.out.println("\n Jusu norimas skaicius yra: " + number1);
//        System.out.println("========================================");

     /*
     Nuskaityti du skaicius ir juos sudeti, panaudojant pries tai sukurta sudeties metoda
      */
//        System.out.println("Iveskite pirmaji skaiciu:");
//        int one = scanner.nextInt();
//        System.out.println("\nIveskite antraji skaiciu:");
//        int two = scanner.nextInt();
//        int sum = one + two;
//        System.out.println("\nJusu ivestu skaiciu suma yra: " + sum);
//        System.out.println("=========================================");

     /*
     Nuskaityti String ir ji atspausdinti.
      */
//        System.out.println("Iveskite norima zodi: ");
//        String name = scanner.nextLine();
//        System.out.println("\nJusu ivestas zodis yra: " + name);
//        System.out.println("=========================================");

     /*
     Nuskaityti du String elementus ir juos atspausdinti
      */
//        System.out.println("Iveskite pirmaji zodi.");
//        String str1 = scanner.nextLine();
//        System.out.println("\nIveskite antraji zodi.");
//        String str2 = scanner.nextLine();
//        System.out.println("\nRezultatas: " + str1 + " " + str2);
//        System.out.println("===========================================");


    }
    Scanner scanner = new Scanner(System.in);

    public int readPrintSumAndReturn(){
        System.out.println("Iveskite pirma skaiciu: ");
        int num1 = scanner.nextInt();
        System.out.println("Iveskite antra skaiciu: ");
        int num2 = scanner.nextInt();
        int sum = num1 + num2;
        System.out.println("Dvieju ivestu skaiciu suma yra: " + sum);
        return sum;

    }

}
