package java3;

import java.util.Scanner;

public class UzduotysNamuDarbai {
    public static void main(String[] args) {

//        printIsPossitive();
//        isVoter();
        differentIfs();


    }
    private static Scanner scanner = new Scanner(System.in);

    /*
    Sukurti programa, kuri atspausdintu ar ivestas skaicius yra neigiamas.
     */
//    public static void printIsPossitive(){
//        int num1;
//        System.out.println("Iveskite skaiciu:");
//        num1 = scanner.nextInt();
//        if(num1 > 0){
//            System.out.println("\nIvestas skaicius yra teigiamas");
//            System.out.println("================================");
//        }else {
//            System.out.println("\nIvestas skaicius yra neigiamas");
//            System.out.println("================================");
//        }
//    }
//    public static void isVoter(){
//        int amzius;
//        System.out.println("Iveskite savo amziu metais balsavimo metu:");
//        amzius = scanner.nextInt();
//        if(amzius <= 0 && amzius >= 100){
//            System.out.println("Ivedete neteisinga amziu, prasau, iveskite savo amziu is naujo: ");
//            System.out.println("================================");
//        }else if(amzius < 18){
//            System.out.println("Jus esate per jaunas balsuoti!");
//            System.out.println("================================");
//        }else if(amzius >= 18 && amzius <100){
//            System.out.println("Jus galite balsuoti! Kam skirsite savo balsa?");
//            System.out.println("================================");
//        }
//    }

    /*
    Sukurti programa, kuri spausdintu atskirus pranesimus jeigu ivestas skaicius:
    (salygos uzduotyje)
     */
    public static void differentIfs(){
        int number;
        System.out.println("Iveskite norima skaiciu: ");
        number = scanner.nextInt();
        if(number < 0){
            System.out.println("Neigiamas skaicius");
            System.out.println("===============================================");
        }else if(number >= 40 && number <= 60){
            System.out.println("Skaicius yra tarp 40 ir 60");
            System.out.println("===============================================");
        }else if(number > 100){
            System.out.println("Skaicius didesnis negu 100");
            System.out.println("===============================================");
        }else {
            System.out.println("Ivedete negalima skaiciu, bandykite dar karta");
            System.out.println("===============================================");
        }
    }

}
