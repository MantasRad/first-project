package java3;

public class UzduotysSalygosSakiniaiIrMetodai {
    public static void main(String[] args) {

        //sukuriamas skaicius patikrinimui:
        int skaicius = 10;


        printBool(checkIfNumberIsNegative(-890));
        printBool(checkIfNumberIsNegative(skaicius));

        printBool(checkIfNumberIs3Digits(4900));
        printBool(checkIfNumberIs3Digits(skaicius));

        ifInRangeOf(9,7, 11);
        ifInRangeOf(10, -10, -4);




    }
    /*
    Sukurti boolean grazinanti metoda pavadinimu checkIfNumberIsNegative. Metodas turi patikrinti ar skaicius neigiamas
    ir grazinti rezultata. Pagal gauta rezultata atspausdinti pranesima.
     */
    public static boolean checkIfNumberIsNegative(int number){
        System.out.println("Iskviestas neigiamumo patikrinimo metodas skaiciui " + number);
        if(number < 0){
            return true;
        }else {
            return false;
        }
//        return (number < 0) ? true : false;
    }

    /*
    Sukurti metoda, kuris patikrina ar paduotas skaicius yra trizenklis
     */
    public static boolean checkIfNumberIs3Digits(int number){
        System.out.println("Iskviestas metodas patikrinti ar skaicius " + number + " yra is 3 skaitmenu");
        String nrStr = "" + Math.abs(number);
        return (nrStr.length() == 3) ? true : false;
    }

    /*
    Sukurti metoda, kuris priima tris int parametrus(sk - skaicius patikrinimui, r1, r2 - skaiciai nusakantys rezius)
    ir grazina boolean kintamaji.
     */
    public static boolean ifInRangeOf(int sk, int r1, int r2){
        System.out.println("Iskviestas metodas patikrinti ar sveikas skaicius " + sk + " yra tarp " + r1 + " ir " + r2);
        if(r1 >= r2){
            System.out.println("Antras rezis yra didesnis uz pirma rezi arba jam lygus, neteisingai ivesti reziai");
            return false;
        }else if(sk >= r1 && sk <= r2){
            System.out.println("Skaicius " + sk + " yra tarp " + r1 + " ir " + r2);
            return true;
        } else {
            System.out.println("Skaiciaus nera atitinkamuose reziuose");
        }return false;
    }

    // tiesiog printeris

    public static void printBool(boolean isPositive){
        System.out.println((isPositive) ? "tiesa" : "netiesa");
        System.out.println("===============================================================");
    }

}

