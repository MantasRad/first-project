package javaExercises40;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Scanner;
import java.io.Console;
import java.io.File;

public class ExercisesTo40 {
    public static void main(String[] args) {

        /*
        20. Write a Java program to convert a decimal number to hexadecimal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a decimal number:");
//
//        int input = scanner.nextInt();
//
//        System.out.println("Your decimal number, converted to hexadecimal is: " + Integer.toHexString(input));
//        System.out.println("===============================================================================");

        /*
        21. Write a Java program to convert a decimal number to octal number.
        The octal numeral system, or oct for short, is the base-8 number system, and uses the digits 0 to 7.
        Octal numerals can be made from binary numerals by grouping consecutive binary digits into groups of three
        (starting from the right). For example, the binary representation for decimal 74 is 1001010.
        Two zeroes can be added at the left: (00)1 001 010, corresponding the octal digits 1 1 2, yielding the octal
        representation 112.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a decimal number:");
//
//        int input = scanner.nextInt();
//
//        System.out.println("Your decimal number, converted to octal is: " + Integer.toOctalString(input));
//        System.out.println("===========================================================================");

        /*
        22. Write a Java program to convert a binary number to decimal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a binary number you want to convert to decimal:");
//
//        String binaryString = scanner.nextLine();
//
//        System.out.println("Your binary number, converted to decimal is: " + Integer.parseInt(binaryString,2));
        // radix atspindi kokios rusies Stringa mes paduodame (jei binarinis - 2, desimtainis - 10, sesioliktainis - 16)
        //ctrl + peles kairysis mygtukas ant parseInt ir suzinom dokumentacija.

        /*
        23. Write a Java program to convert a binary number to hexadecimal number.

        int decimal = Integer.parseInt(binaryStr,2);
        String hexStr = Integer.toString(decimal,16);
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a binary number you want to convert to hexadecimal: ");
//
//        String binaryString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(binaryString,2);
//        String hexString = Integer.toString(decimal,16);
//
//        System.out.println("Your binary number, converted to hexadecimal is: " + hexString);

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a binary number you want to convert to hexadecimal number:");
//
//        String binaryString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(binaryString,2);
//        String hexString = Integer.toHexString(decimal);
//
//        System.out.println("Your binary number, converted to hexadecimal is: " + hexString);

        /*
        24. Write a Java program to convert a binary number to octal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a binary number, you want to convert to octal number:");
//
//        String binaryString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(binaryString,2);
//        String octString = Integer.toString(decimal,8);
//        /* arba String octString = Integer.toOctalString(decimal); */
//
//        System.out.println("Your binary number, converted to octal number is: " + octString);

        /*
        25. Write a Java program to convert a octal number to a decimal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter a octal number you want to convert to a decimal number");
//
//        String octString = scanner.nextLine();
//
//        System.out.println("Your octal number, converted to decimal is: " + Integer.parseInt(octString,8));

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter any octal number: ");
//
//        String octString = scanner.nextLine();
//
//        System.out.println("Octal number, converted to a decimal number is: " + Integer.parseInt(octString,8));

        /*
        26. Write a Java program to convert a octal number to binary number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter any octal number you want to convert to binary:");
//
//        String octString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(octString,8);
//        String octBinary = Integer.toBinaryString(decimal);
//
//        System.out.println("Your octal number, converted to binary number is: " + octBinary);

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter any octal number: ");
//
//        String octString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(octString,8);
//        String octBinary = Integer.toString(decimal,2);
//        //arba String octBinary = Integer.toBinaryString(decimal);
//
//        System.out.println("Your octal to binary: " + octBinary);

        /*
        27. Write a Java program to convert a octal number to hexadecimal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter any octal number:");
//
//        String octString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(octString,8);
//        String octHex = Integer.toHexString(decimal);
//
//        System.out.println("Your octal number, converted to hexa is: " + octHex);

        /*
        28. Write a Java program to convert a hexadecimal number to a decimal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please Enter any hexadecimal number:");
//
//        String hexStriong = scanner.nextLine();
//
//        int decimal = Integer.parseInt(hexStriong,16);
//
//        System.out.println("Your hexadecimal number converted to decimal is: " + decimal);

        /*
        29. Write a Java program to convert a hexadecimal number to a binary number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter any hexadecimal number:");
//
//        String hexString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(hexString,16);
//        String hexBinary = Integer.toBinaryString(decimal);
//
//        System.out.println("Your hexadecimal number, converted to binary number is: " + hexBinary);

        /*
        30. Write a Java program to convert hexadecimal number to a octal number.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter any hexadecimal number:");
//
//        String hexString = scanner.nextLine();
//
//        int decimal = Integer.parseInt(hexString,16);
//        String hexOctal = Integer.toOctalString(decimal);
//
//        System.out.println("Your hexadecimal number converted to a octal number is: " + hexOctal);

        /*
        31. Write a Java program to chech whether Java is installed  on your computer.
         */
//        System.out.println("\nJava Version: "+System.getProperty("java.version"));
//        //java version:
//        System.out.println("Java version: "+System.getProperty("java.version"));
//        //
//        System.out.println("\nJava Runtime Version: "+System.getProperty("java.runtime.version"));
//        //Java runtime version.
//        System.out.println("Java Runtime Version: " +System.getProperty("java.runtime.version"));
//        //
//        System.out.println("\nJava Home: "+System.getProperty("java.home"));
//        //Java home.
//        System.out.println("Java Home: " +System.getProperty("java.home"));
//        //
//        System.out.println("\nJava Vendor: "+System.getProperty("java.vendor"));
//        //java Vendor.
//        System.out.println("Java Vendor: "+System.getProperty("java.vendor"));
//        //
//        System.out.println("\nJava Vendor URL: "+System.getProperty("java.vendor.url"));
//        //Java Vendor URL.
//        System.out.println("Java Vendor URL: "+System.getProperty("java.vendor.url"));
//        //
//        System.out.println("\nJava Class Path: "+System.getProperty("java.class.path")+"\n");
//        //Java Class Path.
//        System.out.println("Java Class Path: "+System.getProperty("java.class.path"));

        /*
        32. Erite a Java program to compare two numbers (25 and 39).
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter first number:");
//        int one = scanner.nextInt();
//
//        System.out.println("Enter second number:");
//        int two = scanner.nextInt();
//
//        if (one == two)
//            System.out.println(one + " == " + two);
//        if (one != two)
//            System.out.println(one + " != " + two);
//        if (one > two)
//            System.out.println(one + " > " + two);
//        if (one >= two)
//            System.out.println(one + " >= " + two);
//        if (one < two)
//            System.out.println(one + " < " + two);
//        if (one <= two)
//            System.out.println(one + " <= " + two);

        /*
        33. Write a Java program to compute the sum of the digits of an integer.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter a two digit number:");
//
//        Long num = scanner.nextLong();
//        System.out.println("The sum of the digits is: " + sumDigits(num));

        /*
        34. Write a Java program to compute the area of a hexagon.
        Area of a hexagon = (6 * s^2)/(4*tan(π/6))
        where s is the length of a side
         */

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter the length of a hexagon side: ");
//
//        double s = scanner.nextInt();
//
//        double area = (6 * Math.pow(s,2) / (4*Math.tan(Math.PI / 6)));
//
//        System.out.println("The Area of a hexagon is: " + area);

        /*
        35. Write a Java program to compute the area of a polygon.
        Area of a polygon = (n*s^2)/(4*tan(π/n))
        where n is n-sided polygon and s is the length of a side
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter n, to say how many sides does polygon have:");
//        double n = scanner.nextInt();
//        System.out.println("Please enter s, to say the length of a side:");
//        double s = scanner.nextInt();
//        double area = (n * Math.pow(s,2) / (4 * Math.tan(Math.PI / n)));
//
//        System.out.println("The area of a polygon is: " + area);

        /*
        36. Write a Java program to compute the distance between two points on the surface of Earth.
         */
        /*
        37. Write a Java program to reverse a string.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter your sentence: ");
//
//        String string = scanner.nextLine();
//        String reverse = new StringBuffer(string).reverse().toString();
//        System.out.println("\n " + reverse);

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter your sentence: ");
//
//        String string = scanner.nextLine();
//        String reverse = new StringBuffer(string).reverse().toString();
//        System.out.println("\nYour sentence printed in reverse is: " + reverse);
//        String reverse1 = new StringBuffer(reverse).reverse().toString();
//        System.out.println(reverse1);

        /*
        38.Write a Java program to count the letters, spaces, numbers and other characters of an input.
         */
        //suskaiciuoja tarpus tarp zodziu:
//        String bar = " ba jfjf jjj j   ";
//        String[] split = bar.split( " " );
//        System.out.println( split.length );

        /*
        41. Write a Java program to print the ascii value of a given character.
         */
//        String name = "Mantas";
//        char character = name.charAt(1); // this gives the character 'a'
//
//        int ascii = (int) character; // ascii is now 97.
//        System.out.println("ASCII code for this letter is: " + ascii);

        /*
        42. Write a Java program to input and display your password.
         */
        //import Console pirmiausiai.

        /*
        43.
         */
        /*
        44.
         */
        /*
        45. Write a Java program to find the size of a specific file.
         */
        /*
        46.
         */
        /*
        47.
         */
        /*
        48. Write a Java program to prit the odd numbers from 1 to 99. Prints one number per line.
         */
//        for (int i = 1; i <= 99; i++){
//            if (i % 2 != 0){
//                System.out.println(i);
//            }
//        }
//        for (int i = 1; i <= 99; i+=2){
//            System.out.println(i);
//        }
        /*
        49. Write a Java project to accept a number and check the number is even or not. Prints 1 if the number is even
        or 0 if the number is odd.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter any number of your choice");
//
//         int number = scanner.nextInt();
//         if (number % 2 == 0){
//             System.out.println("1.! Your number is even.");
//         }else {
//             System.out.println("0.! Your number is odd.");
//         }
        /*
        50. Write a Java program print numbers between 1 and 100 which are divisible by 3,5 and both
         */
//        System.out.println("Numbers divided by 3: ");
//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0)
//                System.out.print(i + ", ");
//        }
//            System.out.println("\nNumbers divided by 3: ");
//            for (int i = 1; i <= 100; i++) {
//                if (i % 5 == 0)
//                    System.out.print(i + ", ");
//            }
//        System.out.println("\nNumbers divideb by 3 and 5: ");
//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0 && i % 5 == 0)
//                System.out.print(i + ", ");

        /*
        51. Write a Java program to convert a String to an Integer.
         */
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Please enter any number:");
//
//        String string = scanner.nextLine();
//
//        int string1 = Integer.parseInt(string);
//
//        System.out.println(string1);


        }





    /*
    33.1. sukuriame atskira objekta - sumDigits.
     */
//    public static int sumDigits(long num) {
//        int sum = 0;
//        while (num != 0) {
//            sum += num % 10;
//            num /= 10;
//        }
//        return sum;
//    }
}
