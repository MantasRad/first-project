public class AsmensKortele {

    public static void main(String args[]){

        String name = "Mantas";
        String lastname = "Radziulis";
        int year = 1988;
        int month = 2;
        int day = 15;
        String personalcode = "123456789";

        System.out.println("------------------------");
        System.out.println("     Asmens Kortele     ");
        System.out.println();
        System.out.println("Vardas: "+ name + " ; Pavarde " + lastname);
        System.out.println("Gimimo data: " + year + month + day);
        System.out.println("Asmens kodas: " + personalcode);
        System.out.println();
        System.out.println("------------------------");
    }

}
