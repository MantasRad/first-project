package java5;

import java.util.Scanner;

/*
Created by mrad on 10/23/2017.
 */

public class MinuteConverter {

    Scanner scanner = new Scanner(System.in);

    /*
    Sukurti atskira minuciu konvertavimo klase, kuri atspausdintu kiek valandu ir dienu yra pagal paduota minuciu
    skaiciu.
     */
//    public void fromMinutestoHoursConverter(){
//        int mins;
//        System.out.println("Iveskite kiek minuciu noresite konvertuoti i valandas: ");
//        mins = scanner.nextInt();
//        int valandos = mins/60;
//        int likusiosMinutes = mins % 60;
//        System.out.println("Jusu ivestu minuciu kiekis - " + mins + " ,atitinka " + valandos + " valandas ir turi " + likusiosMinutes + " minuciu likuti." );
//    }
//    public void fromMinutesToDaysConverter(){
//        int mins;
//        System.out.println("Ibeskite kiek minuciu noretumete konvertuoti i dienas:");
//        mins = scanner.nextInt();
//        int valandos = mins/60;
//        int dienos = valandos / 24;
//        int likusiosMin = mins % 60;
//        System.out.println("Jusu ivestas minuciu kiekis atitinka: " + valandos + " valandas " + dienos + " dienas ir turi " + likusiosMin + " minuciu likuti!" );
//    }
//    public void fromMinutesConverter(){
//        int mins;
//        System.out.println("Iveskite minuciu kieki:");
//        mins = scanner.nextInt();
//        int dienos = mins / 1440;
//        int valandos = mins % 1440 / 60;
//        int likusiosMin = mins % 1440 % 60;
//        System.out.println("Jusu ivestas minuciu kiekis atitinka: " + dienos + " dienas " + valandos + " valandas " + likusiosMin + " minutes.");
//
//    }
    public void toMinuteConverter(){
        System.out.println("Konvertuosime dienas, valandas ir minutes i minutes:");
        System.out.println("=====================================================");
        System.out.println("\nIveskite dienas!");
        int days;
        days = scanner.nextInt();
        System.out.println("\nIveskite valandas!");
        int hours;
        hours = scanner.nextInt();
        System.out.println("\nIveskite minutes!");
        int mins;
        mins = scanner.nextInt();
        System.out.println(days*1440 + hours*60 + mins);
    }


    public static void main(String[] args) {

        MinuteConverter uzd = new MinuteConverter();

//        uzd.fromMinutestoHoursConverter();
//        uzd.fromMinutesToDaysConverter();
//        uzd.fromMinutesConverter();
        uzd.toMinuteConverter();
    }


}
