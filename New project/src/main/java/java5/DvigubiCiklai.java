package java5;

/*
Created by mrad on 10/23/2017.
 */

public class DvigubiCiklai {

    /*
    1. Sukurti metoda, kuris pagal paduotus 2 int parametrus atspausdintu atitinkamo dydzio staciakampi.
     */
//    public static void printStaciakampis(int ilgis, int plotis){
//        String str = "";
//        for(int i = 0; i < ilgis; i++){
//            str = str + "#"; // arba str += "#";
//        }
//        for(int j = 0; j < plotis; j++){
//            System.out.println(str);
//        }
//    }

    /*
    2. Sukurti metoda, kuris pagal 2 int parametrus atspausdintu staciakampi su poslinkiu.
     */
//    public static void printStaciakampisSuPoslinkiu(int ilgis, int plotis){
//        String str = "";
//        for (int i = 0; i < ilgis; i++){
//            str += "#";
//        }
//        for (int j = 0; j < plotis; j++){
//            if(j % 2 == 0){
//                System.out.println(" " + str.substring(0, str.length() - 2));
//            }else {
//                System.out.println(str);
//            }
//        }
//    }

    /*
    3. Sukurti metoda, kuris atspausdintu eglute.
     */
//    public static void christmasTreeWithoutOffset(int height){
//        String str = "";
//        for(int i = 0; i < height; i++){
//            for(int j = 0; j < height; j++) {
//                str += "*";
//                System.out.println(str);
//            }
//        }
//
//    }
//    4. public static void christmasTreeWithOffset(int height){
//        String str = "";
//        int offset = height;
//        for(int i = 0; i < height; i++){
//
//            for(int j = 0; j < offset; j++){
//                System.out.println(" ");
//            }
//            for(int j = 0; j < 2*i-1; j++){
//                str += "*";
//                System.out.print(str);
//            }
//            offset--;
//        }
//    }
    /*
    5. Daugybos lentele.
     */
//    public static void printMultiplyTable(){
//        for(int i = 0; i <= 10; i++){
//            for (int j = 0; j <= 10; j++){
//                System.out.println(i + " X " + j + " = " + i*j);
//            }
//            System.out.println("=============================");
//        }
//    }
//    public static void printMultiplyTable(){
//        for(int i = 1; i <= 10; i++){
//            for (int j = 0; j <= 10; j++){
//                System.out.println(i + " X " + j + " = " + i*j);
//            }
//            System.out.println("===============================");
//        }
//    }
//    public static void printMultiplyTable(){
//        for (int i = 1; i <= 10; i++){
//            for(int j = 0; j <= 10; j++){
//                System.out.println(i + " X " + j + " = " + i*j);
//            }
//            System.out.println("==============================");
//        }
//    }


    /*
    6.
     */
    public static void matrixPrinter(int [][] matrix){
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }
    public static int [][] matrixFiller(){
        int [][] matrix = new int[10][10];
        for (int aukstis = 0; aukstis < 10; aukstis++){
            for (int ilgis = 0; ilgis < 10; ilgis++){
                matrix[ilgis][aukstis] = ilgis + aukstis +1;
            }
        }
        return matrix;
    }






    public static void main(String[] args) {

//        printStaciakampis(10, 5);
//        System.out.println("\n ------------------------------");
//        printStaciakampisSuPoslinkiu(6,5);
//        System.out.println("\n -------------------------------");
        matrixPrinter(matrixFiller());
//
//        christmasTreeWithoutOffset(2);
//        christmasTreeWithOffset(3);
//        printMultiplyTable();

    }
}
