package testexamples;

public class KavosMasina {
    private int vandensKiekis;
    private int pupeliuKeikis;
    
    //!!! bandom kurti patikrinima ar uztenka produktu
//    private int produktuKiekis;

    public void uzpildytiPupeles(int pupeliuKiekis) {
        this.pupeliuKeikis += pupeliuKiekis;

    }

    public void uzpildytiVandens(int vandensKiekis) {
        this.vandensKiekis += vandensKiekis;
    }

    public void darytiJuodaKava() {
        vandensKiekis -= 1;
        pupeliuKeikis -= 20;
        //!!! bandom kurti patikrinima ar uztenka produktu
//        produktuKiekis -= 21;


    }

    public int getVandens() {
        return vandensKiekis;
    }

    public int getVandensKiekis() {
        return vandensKiekis;
    }

    public void setVandensKiekis(int vandensKiekis) {
        this.vandensKiekis = vandensKiekis;
    }

    public int getPupeliuKeikis() {
        return pupeliuKeikis;
    }

    public void setPupeliuKeikis(int pupeliuKeikis) {
        this.pupeliuKeikis = pupeliuKeikis;
    }

    //!!! bandom kurti patikrinima ar uztenka produktu
//    public void patiktintiArUztenkaProduktu(int produktuKiekis) {
//        this.produktuKiekis += produktuKiekis;
//    }

    //!!! bandom kurti patikrinima ar uztenka produktu
//    public int getProduktuKiekis() {
//        return produktuKiekis;
//    }
}
