package Treciadienis11;

public class Sesta {

//    kintamasis - vieta ta, kur padesim sukurta objekta.

    private Septinta septintasObjektas;


    public Sesta(){

//        sukurta objekta 'new Septinta()' padedam i kintamaji.

        septintasObjektas = new Septinta();


    }
    public int liepkRektiSeptintamObjektui(){
        int skaicius = septintasObjektas.rekti();
        return skaicius;

    }
    public void imkSkaiciu(int i) {
        septintasObjektas.imkSkaiciu(i);
    }
    public Fantas imkFanta (Fantas fantas){
        Fantas skardine = septintasObjektas.imkFanta(fantas);
        return skardine;
    }

}
