package Treciadienis11;

public class Penkta {
    private Sesta sestasObjektas;

    public Penkta(){
        sestasObjektas = new Sesta();
    }
    public void liepkRektiSeptintamObjektui(){

        sestasObjektas.liepkRektiSeptintamObjektui();
    }
    public void imkSkaiciu(int i) {
        sestasObjektas.imkSkaiciu(i);
    }
    public Fantas imkFanta (Fantas fantas){
        Fantas skardine = sestasObjektas.imkFanta(fantas);
        return skardine;
    }
}
