package Treciadienis11;

public class Trecia {

    private Ketvirta ketvirtasObjektas;

    public Trecia(){
        ketvirtasObjektas = new Ketvirta();
    }

    public void liepkRektSeptintamObjektui(){

        ketvirtasObjektas.liepkRektiSeptintamObjektui();
    }
    public void imkSkaiciu(int i) {
        ketvirtasObjektas.imkSkaiciu(i);
    }
    public Fantas imkFanta (Fantas fantas){
        Fantas skardine = ketvirtasObjektas.imkFanta(fantas);
        return skardine;
}
}
