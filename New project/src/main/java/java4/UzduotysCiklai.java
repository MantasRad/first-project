package java4;

import java.util.Scanner;


public class UzduotysCiklai {
    public static void main(String[] args) {

        /*
        Penkis kartus atspausdinti "Hello World!" panaudojant for cikla.
         */
//        for(int i = 0; i < 5; i++){
//            System.out.println("Hello, World! " + (i +1));
//        }
//        System.out.println("=========================");
        /*
        Penkis kartus atspausdinti "Hello, World!" panaudojant while cikla.
         */
//        int i = 0;
//        while (i < 5){
//            i++;
//            System.out.println("Hello, World! " + i);
//        }
//        System.out.println("=========================");
        /*
        Penkis kartus atspausdinti "Hello, World!" panaudojant do-while cikla.
         */
//        int i = 0;
//        do {
//            i++;
//            System.out.println("Hello, World! " + i);
//        }while (i < 5);
//        System.out.println("==============================");
        /*
        Nuskaityti ivesta skaiciu ir tiek kartu atspausdinti "Hello, World!" panaudojant for cikla.
         */
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Kiek kartu norite ivesti zodziu jungini Hello, World?");
//        int number = scanner.nextInt();
//
//        for (int i = 0; i < number; i++){
//            System.out.println("Hello, World! " + (i +1));
//        }
        /*
        Nuskaityti ivesta skaiciu ir tiek kartu atspausdinti "Hello, World!" panaudojant while cikla.
         */
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Kiek kartu norite ivesti zodziu jungini Hello, World!?");
//
//        int num = scanner.nextInt();
//
//        int i = 0;
//        while (i < num){
//            i++;
//            System.out.println("Hello, World! " + i);
//        }
        /*
        Nuskaityti ivesta skaiciu ir tiek kartu atspausdinti "Hello, World!" panaudojant do-while cikla.
         */
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Kiek kartu norite atspausdinti zodziu jungini Hello, World!?");
//        int sk = scanner.nextInt();
//
//        int i = 0;
//        do {
//            i++;
//            System.out.println("Hello, Mantai :) " + i);
//        }while (i < sk);

        /*
        Atspausdinti skaicius 1-50 panaudojant for cikla.
         */
//        for(int i = 1; i <= 50; i++){
//            System.out.println(i);
//        }
        /*
        Atspausdinti skaicius 1-50 panaudojant while cikla.
         */
//        int i = 0;
//        while (i < 50){
//            i++;
//            System.out.println(i);
//        }
        /*
        Atspausdinti skaicius 1-50 panaudojant do-while cikla.
         */
//        int i = 0;
//        do{
//            i++;
//            System.out.println(i);
//        }while (i <50);

        /*
        Atspausdinti skaiciu suma nuo 1-100 panaudojant for cikla.
         */
//        int suma = 0;
//        for(int i = 1; i <= 100; i++){
//            suma = suma + i;
//        }
//        System.out.println("Suma yra: " + suma);

        /*
        Atspausdinti skaiciu suma nuo 1-100 panaudojant while cikla.
         */
//        int i = 0;
//        int suma = 0;
//        while (i < 100){
//            i++;
//            suma = suma + i;
//        }
//        System.out.println("Rezultatas yra: " + suma);

        /*
        Atspausdinti skaiciu suma nuo 1-100 panaudojant do-while cikla.
         */
        int i = 0;
        int suma = 0;
        do{
            i++;
            suma = suma + i;
        }while (i < 100);
        System.out.println("Rezultatas: " + suma);
    }

}
