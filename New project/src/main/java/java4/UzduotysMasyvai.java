package java4;

public class UzduotysMasyvai {
    public static void main(String[] args) {

        /*
        Sukurti programa, kuri isspausdintu visus masyvo inicializuoto kaip "int masyvas [] ={skaiciai} narius.
         */
//        int masyvas [] = {1, 2, 3, 4, 5};
//        for(int i = 0; i < masyvas.length; i++){
//            System.out.print(masyvas[i] + " ");
//        }
//        int masyvas [] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
//        for (int i = 0; i < masyvas.length; i++){
//            System.out.print(masyvas[i] + " ");
//        }

        /*
        Sukurti programa, kuri atspausdintu masyvo nariu suma.
         */
//        int masyvas [] = {1, 2, 3, 4, 5, 6};
//        int sum = 0;
//
//        for (int i = 0; i < masyvas.length; i++){
//            sum = sum + masyvas[i];
//        }
//        System.out.println("Rezultatas yra: " + sum);

        int masyvas [] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int sum = 0;

        for (int i = 0; i < masyvas.length; i++){
            sum = sum + masyvas[i];
        }
        System.out.println("Rezultatas: " + sum);




    }
}
