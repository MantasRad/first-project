package java4;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MasyvaiNamuDarbai {

    /*
    Sukurti metoda, kuris patikrintu ar du paduoti masyvai yra lygus - pagal ilgi ir pagal reiksmes.
     */

//    1.
// public boolean checkArrayEquality(int [] a, int [] b){
//        return (Arrays.equals(a,b) ? true : false);
//    }
//    public void printIfEqual(boolean masyvaiLygus){
//        System.out.println((masyvaiLygus) ? "Lygus" : "Nelygus");

//   2.
// public boolean checkArraysEquality(int [] a, int [] b){
//        if(Arrays.equals(a,b)){
//            return true;
//        }else {
//            return false;
//        }
//    }
//    public void printIfEqual(boolean masyvaiLygus){
//        if(masyvaiLygus){
//            System.out.println("Lygus");
//        }else {
//            System.out.println("Nelygus");
//        }

//    }

    /*
    2. Sukurti programa, kuri skaitytu sveikus skaicius nuo 0 iki 10 iki tol kol bus ivestas -1 ir tada atspausdintu
    kiek kokiu skaiciu buvo ivesta.
     */

    Scanner scanner = new Scanner(System.in);

    public int searchIntInArray(int num, int [] nums){
        int position = -1;
        for(int i = 0; i < nums.length; i++){
            if(nums [i] == num){
                position = i;
                break;
            }
        }return position;
    }
    public void intCountPrinter(){
        System.out.println("\nPradekite ivedineti sveikus skaicius nuo 0 iki 10, ivede -1 iseisime is programos");
        int scanned = scanner.nextInt();
        ArrayList<Integer> visiSkaiciai = new ArrayList<>();
        visiSkaiciai.add(scanned);
        int[] counter = new int[11];
        counter[scanned]++;
        while (true){
            System.out.println("Iveskite skaiciu:");
            scanned = scanner.nextInt();
            if(scanned == -1){
                System.out.println("0, 1, 2, 3, 4, 5, 6, 7, 8, 9");
                System.out.println("|  |  |  |  |  |  |  |  |  |");
                System.out.println(arrayToString(counter));
                for(Integer i : visiSkaiciai){
                    System.out.println(i);
                }
                break;
            }
            counter[scanned]++;
            visiSkaiciai.add(scanned);
        }
    }
    public String arrayToString(int [] nums){
        String numsStr ="" + nums[0];
        for(int i = 1; i < nums.length; i++){
            numsStr += (", "+ nums[i]);
        }return numsStr;
    }

    public static void main(String[] args) {

        int [] a = {1, 2, 3};
        int [] b = {3, 2, 1};
        int [] c = {1, 2, 3};
        int [] d = {1, 2, 3, 4};

        MasyvaiNamuDarbai uzduotys = new MasyvaiNamuDarbai();
//
//        uzduotys.printIfEqual(uzduotys.checkArrayEquality(a,c));
//        uzduotys.printIfEqual(uzduotys.checkArrayEquality(a,b));
//        uzduotys.printIfEqual(uzduotys.checkArrayEquality(b,d));
//        uzduotys.printIfEqual(uzduotys.checkArrayEquality(d,a));

        uzduotys.intCountPrinter();

        System.out.println(uzduotys.searchIntInArray(3,d));
        System.out.println(uzduotys.arrayToString(c));

    }






}
