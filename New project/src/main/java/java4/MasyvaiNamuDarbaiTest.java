package java4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MasyvaiNamuDarbaiTest {


    public boolean checkArrayEquality(int[] a, int[] b){
        return (Arrays.equals(a,b)) ? true: false;
    }

    public void printIfEqual(boolean masyvaiLygus){
        System.out.println((masyvaiLygus)? "Lygus":"Nelygus");
    }


    public static void main(String[] args) {

        MasyvaiNamuDarbaiTest mO = new MasyvaiNamuDarbaiTest();

        int[] a = {1,2,3};
        int[] b = {1,2,3};
        int[] c = {2,3,4};
        int[] d = {1,2,3,6,9};

        mO.printIfEqual(mO.checkArrayEquality(a,b));
        mO.printIfEqual(mO.checkArrayEquality(b,c));
        mO.printIfEqual(mO.checkArrayEquality(c,d));
        mO.printIfEqual(mO.checkArrayEquality(a,d));

    }
}
