package java4;

import java.util.Scanner;


public class UzduotisSwitchSakinys {
    public static void main(String[] args) {

        UzduotisSwitchSakinys uzduotys = new UzduotisSwitchSakinys();

        Scanner scanner = new Scanner(System.in);

//        uzduotys.checkAge();
//        uzduotys.isKnownInt();

        boolean quit = false;
        int counter = 0;
        int initial = 0;
        uzduotys.printMeniu();
        while (!quit){
            initial = scanner.nextInt();
            switch (initial){
                case 0:
                    uzduotys.printMeniu();
                    break;
                case 1:
                    uzduotys.sumTwoInts();
                    break;
                case 2:
                    uzduotys.multTwoInts();
                    break;
                case 3:
                    uzduotys.sq256();
                    break;
                case 4:
                    uzduotys.have123Selected(counter);
                    break;
                case 5:
                    quit = true;


            }
        }





    }
    /*
    Sukurti paprasta switch sakini su keliom salygom su skaiciais. Pabandyti kas vyksta idejus/ isemus break sakini.
     */
//    Scanner scanner = new Scanner(System.in);
//
//    public void checkAge(){
//        System.out.println("Please enter your age from 1 to 4.");
//        int age = scanner.nextInt();
//        switch (age){
//            case 1:
//                System.out.println("You are to young to enter this lottery.");
//                break;
//            case 2:
//                System.out.println("You are still to young to enter, bring your mother here.");
//                break;
//            case 3:
//                System.out.println("You can enter this lottery. Do you want to win some good stuff?");
//                break;
//            case 4:
//                System.out.println("You`ve won a car. Congrads");
//                break;
//            default:
//                System.out.println("Go home, you are too old for this.");
//                break;
//        }
//    }
    /*
    Sukurti paprasta switch sakini, kuris turetu kelias salygas - jeigu ivestas 1,2,3 arba 5 - tada atspausdintu "Ivestas
    zinomas skaicius: 1, 2, 3 arba 5". Jeigu ivestas bet koks kitas skaicius - atspausdintu "Ivestas nezinomas skaicius".
    Po to - sukurti metoda, kuris mums padetu patogiau atspausdinti "Ivestas zinomas skaicius: x".
     */
//    Scanner scanner = new Scanner(System.in);
//
//    public void isKnownInt(){
//        System.out.println("Iveskite skaiciu patikrinimui:");
//        int num = scanner.nextInt();
//        switch (num){
//            case 1: case 2: case 3: case 5:
//                System.out.println("Jusu ivestas skaicius " + num + " metodui yra zinomas");
//                break;
//            default:
//                System.out.println("Jusu ivestas skaicius " + num + " metodui yra nezinomas");
//                break;
//        }
//    }

    /*
    Sukurti programa su meniu, kuri atspausdintu meniu punktus 1.2.3. ir t.t. su paaiskinimais. Tada paprasytu ivesti
    meniu pasirinkima kaip skaiciu ir panaudojant switch sakini atitinkamai atliktu pagal skaiciu pasirinkta veiksma.
     */
    Scanner scanner = new Scanner(System.in);

    public void printMeniu(){
        System.out.println();
        System.out.println("\tPasirinkite operacija, kuria noretumete atlikti:\n" +
                "==============================================================\n" +
                "\t-> 0 - Atspausdinti meniu\n" +
                "\t-> 1 - Dvieju skaiciu sudetis\n" +
                "\t-> 2 - Dvieju skaiciu daugyba\n" +
                "\t-> 3 - Skaiciaus 256 pakelimas kvadratu\n" +
                "\t-> 4 - Ar buvo pasirinktas punktas 1, 2 arba 3? Kiek kartu?\n" +
                "\t-> 5 - Iseiti is programos\n" +
                "==============================================================");
    }
    public void sumTwoInts(){
        System.out.println("Iveskite pirmaji skaiciu:");
        int one = scanner.nextInt();
        System.out.println("Iveskite antraji skaiciu:");
        int two = scanner.nextInt();
        int sum = one + two;
        System.out.println("Skaiciu " + one + " ir " + two + " suma yra: " + sum);
    }
    public void multTwoInts(){
        System.out.println("Iveskite pirmaji skaiciu: ");
        int one = scanner.nextInt();
        System.out.println("Iveskite antraji skaiciu: ");
        int two = scanner.nextInt();
        int mult = one * two;
        System.out.println("Skaiciu " + one + " ir " + two + "sandauga yra: " + mult);
    }
    public void sq256(){
        System.out.println("Skaicius 256 pakeltas kvadratu yra lygus " + (256 * 256));

    }
    public void have123Selected(int counter){
        System.out.println((counter > 0) ? "Punktas 1,2 arba 3 buvo pasirinktas " + counter + " kartu ": "Ketvirtas punktas yra pirmas, kuri pasirinkote sioje programoje");

    }




}
