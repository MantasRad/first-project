package jpa;

import javax.persistence.*;

@Entity
@Table(name = "ZAIDEJAI")
public class Zaidejas {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "Vardas")
    private String vardas;

    @Column(name = "Pavarde")
    private String pavarde;

    @Column(name = "Amzius")
    private String amzius;

    @Column(name = "Miestas")
    private String miestas;

    @Column(name = "Automobilis")
    private String automobilis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public String getAmzius() {
        return amzius;
    }

    public void setAmzius(String amzius) {
        this.amzius = amzius;
    }

    public String getMiestas() {
        return miestas;
    }

    public void setMiestas(String miestas) {
        this.miestas = miestas;
    }

    public String getAutomobilis() {
        return automobilis;
    }

    public void setAutomobilis(String automobilis) {
        this.automobilis = automobilis;
    }
}
